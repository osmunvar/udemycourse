import 'package:UdemyCourse/consts/theme_data.dart';
import 'package:UdemyCourse/provider/dark_theme_provider.dart';
import 'package:UdemyCourse/screens/buttom_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  DarkThemeProvider themeChangeProvider = DarkThemeProvider();
  @override
  Widget build(BuildContext context) {
    
    return MultiProvider(providers: [
      ChangeNotifierProvider(create: (_) {
        return themeChangeProvider;
      })
    ],
    child: Consumer<DarkThemeProvider>(
      builder: (context, themeData, child) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Flutter Demo',
          theme: Style.themeData(themeChangeProvider.darkTheme, context),
          home: BottomBarScreen(),
        );
      }
    ));
  }
}

