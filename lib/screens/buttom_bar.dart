import 'package:UdemyCourse/consts/my_icons.dart';

import 'search.dart';
import 'user_info.dart';
import 'package:flutter/material.dart';

import 'cart.dart';
import 'feeds.dart';
import 'home.dart';

class BottomBarScreen extends StatefulWidget {
  @override
  _BottomBarScreenState createState() => _BottomBarScreenState();
}

class _BottomBarScreenState extends State<BottomBarScreen> {
  List<Map<String, Object>> _pages;
  int _selectedPageIndex = 0;

  @override
  void initState() {
    _pages = [
      {
        'page': Home(),
      },
      {
        'page': Feeds(),
      },
      {
        'page': Search(),
      },
      {
        'page': Cart(),
      },
      {
        'page': UserInfo(),
      },
    ];
    super.initState();
  }

  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _pages[_selectedPageIndex]['page'],
      bottomNavigationBar: BottomAppBar(
        // color: Colors.white,
        shape: CircularNotchedRectangle(),
        notchMargin: 0.01,
        clipBehavior: Clip.antiAlias,
        child: Container(
          height: kBottomNavigationBarHeight * 0.99,
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border(
                top: BorderSide(
                  color: Colors.orangeAccent,
                  width: 0.5,
                ),
              ),
            ),
            child: BottomNavigationBar(
                onTap: _selectPage,
                backgroundColor: Theme.of(context).primaryColor,
                // ignore: deprecated_member_use
                unselectedItemColor: Theme.of(context).textSelectionColor,
                selectedItemColor: Colors.orangeAccent,
                currentIndex: _selectedPageIndex,
                items: [
                  BottomNavigationBarItem(
                      icon: Icon(MyAppIcons.home, size: 23,),
                      label: 'Home'
                  ),
                  BottomNavigationBarItem(
                      icon: Icon(MyAppIcons.rss, size: 23,),
                      label: 'Feeds'
                  ),
                  BottomNavigationBarItem(
                      activeIcon: null,
                      icon: Icon(null, size: 22,),
                      label: ''
                  ),
                  BottomNavigationBarItem(
                      icon: Icon(MyAppIcons.cart, size: 23,),
                      label: 'Cart'
                  ),
                  BottomNavigationBarItem(
                      icon: Icon(MyAppIcons.user, size: 23,),
                      label: 'User'
                  ),
                ]
            ),
          ),
        ),
      ),
      floatingActionButtonLocation:
      FloatingActionButtonLocation.miniCenterDocked,
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(8.0),
        child: FloatingActionButton(
          hoverElevation: 10,
          splashColor: Colors.orangeAccent,
          tooltip: 'Search',
          elevation: 4,
          child: Icon(MyAppIcons.search),
          onPressed: () => setState(() {
            _selectedPageIndex = 2;
          }),
        ),
      ),
    );
  }
}
