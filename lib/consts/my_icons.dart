import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class MyAppIcons {

  static IconData home = Feather.home;
  static IconData rss = Entypo.rss;
  static IconData cart = Entypo.shopping_cart;
  static IconData user = Entypo.user;
  static IconData search = EvilIcons.search;

}